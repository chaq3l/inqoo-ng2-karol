import { Component, Inject, OnInit, Output, EventEmitter } from '@angular/core';
import { AlbumSearchResponse } from 'src/app/core/model/album';
import { AlbumSearchService } from 'src/app/core/services/album-search/album-search.service';
import { Album } from 'src/app/core/model/album';


@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.scss']
})
export class AlbumSearchComponent implements OnInit {
  results: Album[] = []
  message= ''

  constructor(
    // @Inject(AlbumSearchService)
    private service: AlbumSearchService
  ) { }
    
  
  search(query: string) {
    this.service
      .getAlbums(query)
      .subscribe({
        next: (albums) => this.results = albums,
        error: (error) => this.message = error.message,
      })
  }

  ngOnInit(): void {
  }

  // getSearchingPhrase(phrase:string){
  //   const searchResults = this.service.getAlbumsByName(phrase)
  //   if(searchResults){
  //     this.results = this.service.getAlbumsByName(phrase)
  //   }
  // }

}
