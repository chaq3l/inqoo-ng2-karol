import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { map, catchError, filter, switchMap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

import { Album, Track } from 'src/app/core/model/album';
import { AlbumSearchService } from 'src/app/core/services/album-search/album-search.service';


/* 
  TODO:
  - load this on /music/albums?id=5Tby0U5VndHW0SomYO7Id7
  - show Id in html
  - fetch Alubm from server
  - show album Image, name
  - search results -> click card -> redirects here
*/

@Component({
  selector: 'app-album-details-view',
  templateUrl: './album-details-view.component.html',
  styleUrls: ['./album-details-view.component.scss']
})
export class AlbumDetailsViewComponent implements OnInit {


  SEARCH_PARAM = 'id'
  message = ''
  album? : Album
  id = ''

  @ViewChild('audioRef')
  audioRef!: ElementRef<HTMLAudioElement>

  constructor(
    private service : AlbumSearchService,
    private route : ActivatedRoute,
    private router : Router
    ) { }

  query = this.route.queryParamMap.pipe(
    map(params => params.get(this.SEARCH_PARAM)),
    filter((q): q is string => q !== null),
  )

  results = this.query.pipe(switchMap(query => this.service.getAlbumById(query)),
    catchError((error) => {
      this.message = error.message
      return []
    })
  )
  ngOnInit(): void {
    this.query.subscribe( id => {this.id = id})
    this.results.subscribe( response => {this.album = response})
  }


  //currenTrack?: Track
  playTrack(track: Track) {
    //this.currenTrack = track
    if (this.audioRef.nativeElement) {
      this.audioRef.nativeElement.src = track.preview_url
      this.audioRef.nativeElement.play()
    }
  }
}
