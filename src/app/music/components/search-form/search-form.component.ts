import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  constructor() { }
  searchPhrase : string = ''
  @Output('searchingPhrase') searchingPhrase = new EventEmitter<string>()
  ngOnInit(): void {
  }

  onSearchButtonClick(searchPhrase: string){
    this.searchingPhrase.emit(searchPhrase)
  }

}
