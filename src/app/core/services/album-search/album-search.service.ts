import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators'

import { AlbumSearchResponse, SimpleAlbum, Album } from '../../model/album';
import { AuthService } from '../auth/auth.service'; 

const mockAlbums: SimpleAlbum[] = [
  {
    id: '123', name: 'Album 123', images: [
      { url: 'https://www.placecage.com/c/300/300' }
    ]
  },
  {
    id: '234', name: 'Album 234', images: [
      { url: 'https://www.placecage.com/c/400/400' }
    ]
  },
  {
    id: '345', name: 'Album 345', images: [
      { url: 'https://www.placecage.com/c/460/300' }
    ]
  },
  {
    id: '456', name: 'Album 456', images: [
      { url: 'https://www.placecage.com/c/300/300' }
    ]
  },
  {
    id: '567', name: 'Album 567', images: [
      { url: 'https://i1.sndcdn.com/artworks-000623498662-5hlgmp-t500x500.jpg' }
    ]
  },
  {
    id: '678', name: 'Album 678', images: [
      { url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkNdW_JHrwhgHKc9Nit8GnAfaCCBrSYa4aeQ&usqp=CAU ' }
    ]
  },


 
]
interface AlbumResponse {
  albums: {
    items: AlbumSearchResponse[]
  }
}
@Injectable({
  providedIn: 'root'
})
export class AlbumSearchService {

  
  constructor(private http : HttpClient, private auth: AuthService) { }

  // holoyis165@bulkbye.com
  // placki777

  getAlbumById(id: string) {
    return this.http
      .get<Album>(`https://api.spotify.com/v1/albums/` + id, {
        headers: {
          'Authorization': 'Bearer ' + this.auth.getToken()
        }
      })
  }

  getAlbums(query: string) {
    return this.http
      .get<AlbumSearchResponse>(`https://api.spotify.com/v1/search`, {
        params: { q: query, type: 'album' },
        headers: {
          'Authorization': 'Bearer ' + this.auth.getToken()
        }
      }).pipe(
        map(resp => resp.albums.items),
        catchError(error => {
          if (!(error instanceof HttpErrorResponse)) {
            console.error(error);
            return throwError(new Error('Unexpected error'))
          }
          if (error.status == 401) {
            setTimeout(() => { this.auth.login() }, 2000)
            return throwError(new Error('Session expired. Logging you out'))
          }
          return throwError(new Error(error.error.error.message))
        })
      )
  }

  getAlbumsByName(name:string){
    const results = mockAlbums.filter(
      item => {
        return item.name.toLocaleLowerCase().includes(name.toLocaleLowerCase())
      })      
    return results
  }
}
