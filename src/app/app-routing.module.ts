import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/playlists',
    pathMatch: 'full' // prefix
  },
  {
    path: 'music',
    loadChildren: () =>
      import('./music/music.module')
        .then(m => m.MusicModule)
  },
  {
    path: '**', // wildcard - match all,
    // component: PageNotFoundComponent
    redirectTo: 'music'
  }
  // ....PlaylistsRoutingModule
];

// const appRoutes: Routes = [
//   { path:'', redirectTo: '/playlist', pathMatch: 'full'},
//   //{ path:'recipe', loadChildren: './recipes/recipes.module#RecipesModule'}
//   { path:'details', loadChildren: () => import('/playlists/components/playlist-details')
//   .then(m => m.RecipesModule)},
//   { path:'editor', loadChildren: () => import('./playlists/components/playlist-editor')
//   .then(m => m.ShoppingModule)},
//   { path:'auth', loadChildren: () => import('./auth/auth.module')
//   .then(m => m.AuthModule)},
//   { path: '**', redirectTo: '/playlists'}
// ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
