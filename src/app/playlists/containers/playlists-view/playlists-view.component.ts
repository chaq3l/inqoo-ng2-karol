import { Component, OnChanges, OnInit } from '@angular/core';
import { Playlist, PlaylistClass, UndefPlaylist } from '../../components/playlists-list/Playlist';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit, OnChanges {

  constructor() { }

  
  playlists : Playlist[] = [
    new PlaylistClass('123', 'Playlist 123', true, 'my favourite playlist'),
    new PlaylistClass('234', 'Playlist 234', false, 'my favourite playlist'),
    new PlaylistClass('345', 'Playlist 345', true, 'my favourite playlist'),
    new PlaylistClass('456', 'Playlist 456', false, 'my favourite playlist'),
  ]

  selected? : Playlist
  draft? : Playlist
  
  mode: 'details' | 'edit' | 'create' = 'details'
  
  ngOnInit(): void {
    this.selected = this.playlists[0]
  }

  deleteElement(deleteEvent:string) {
    this.playlists = this.playlists.filter(item=> {
     return item?.id !==  this.selected?.id
    })
    this.selected = new UndefPlaylist()     
  }

  editElement(playlist: Playlist) {   
    this.playlists = this.playlists.map(
      item=> {
        if(item!==undefined && playlist !== undefined){
          return item.id === playlist.id ? playlist : item 
        }
        else{
          return item
          }
    })
    this.selected = playlist
  }

  addElement(playlist: Playlist) { 
    let newPlaylist = playlist
    if(newPlaylist){
      newPlaylist.id = String(Number(this.playlists[this.playlists.length-1]?.id)+1)
    }else{}
    if(newPlaylist?.id !== undefined){
      this.playlists.push(new PlaylistClass(
        newPlaylist?.id,
        newPlaylist?.name, 
        newPlaylist?.publicPlaylistAttr, 
        newPlaylist?.description))
      //console.log(this.playlists)
    }
    
  }
  
  ngOnChanges(): void {
    if(this.selected!==undefined){
      this.draft = new PlaylistClass(
      this.selected.id,
      this.selected.name,
      this.selected.publicPlaylistAttr,
      this.selected.description)
    }
    //this.selectedChange(this.selected)
  }
 
  selectedChange(playlist :Playlist) {
    this.selected = playlist
  }

}

// [{
//   id: '123',
//   name: 'Playlist 123',
//   publicPlaylistAttr: true,
//   description: 'my favourite playlist'
// }, {
//   id: '234',
//   name: 'Playlist 234',
//   publicPlaylistAttr: false,
//   description: 'my favourite playlist'
// }, {
//   id: '345',
//   name: 'Playlist 34',
//   publicPlaylistAttr: true,
//   description: 'my favourite playlist'
// }]