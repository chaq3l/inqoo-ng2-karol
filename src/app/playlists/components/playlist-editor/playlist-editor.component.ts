import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ElementRef, OnChanges } from '@angular/core';
import { Playlist, PlaylistClass } from '../playlists-list/Playlist';

@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.scss']
})
export class PlaylistEditorComponent implements OnInit, OnChanges {

  constructor() { }

  @Output('mode') mode = new EventEmitter<'details'>()
  @Output('savedElement') elementToSave = new EventEmitter<Playlist>()
  @Output('newElement') elementToAdd = new EventEmitter<Playlist>()
  @ViewChild('itemNameRef', {static: false}) itemName!: ElementRef;


  @Input('selectedPlaylist') playlist : Playlist = {
    id: '123',
    name: 'Playlist',
    publicPlaylistAttr: false,
    description: 'my favourite playlist'
  }

  editOrSaveMode : string = 'Save changes'

  draft ?: Playlist
  ngOnChanges(): void{    
      this.draft = new PlaylistClass(
      this.playlist!.id,
      this.playlist!.name,
      this.playlist!.publicPlaylistAttr,
      this.playlist!.description)

      if(this.draft.id != 'undefined'){
        this.editOrSaveMode = 'Save changes'              
      }else{
        this.editOrSaveMode = 'Add element'           
      }
  }
   
  ngOnInit(): void {
    
  }

  switchToDetailsMode() {
    this.mode.emit('details')
  }

  onSaveElement() {  
    if(this.draft)
    if(this.draft.id != 'undefined'){
      this.elementToSave.emit(
        {...this.draft})              
    }else{
      this.elementToAdd.emit(
        {...this.draft})            
    }
    
    
    this.switchToDetailsMode()
  }

}
