export interface IPlaylist {
  id: string;
  name: string;
  publicPlaylistAttr: boolean;
  description: string;
}

export class UndefPlaylist implements IPlaylist {
  id: string;
  name: string;
  publicPlaylistAttr: boolean;
  description: string;

  constructor(){
    this.id = 'undefined'
    this.name = ''
    this.publicPlaylistAttr = false
    this.description = ''
  }
}

export class PlaylistClass implements IPlaylist {
  id: string;
  name: string;
  publicPlaylistAttr: boolean;
  description: string;
  constructor( 
    id: string,  
    name: string, 
    publicPlaylistAttr: boolean, 
    description: string) {
  this.id = id
  this.name = name
  this.publicPlaylistAttr = publicPlaylistAttr
  this.description = description
  }
}

export type Playlist = PlaylistClass | UndefPlaylist | undefined
