import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Playlist, UndefPlaylist } from './Playlist';

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss']
})
export class PlaylistsListComponent implements OnInit {

  constructor() { }

  @Input('selected') selectedItem !: Playlist

  @Input('items') playlists : Playlist[] = []
   
  @Output('selected') selectetItemChange = new EventEmitter<Playlist>()

  //nazwaChange to konwencja dzięki której możemy używać [(input/output)]

  onSelect(playlist: Playlist){
    this.selectedItem = playlist === this.selectedItem ? new UndefPlaylist() : playlist
  
    if(this.selectedItem !== undefined){
      this.selectetItemChange.emit(this.selectedItem)
    }
  }
  ngOnInit(): void {
  }

}
