import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Playlist } from '../playlists-list/Playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  constructor() { }
  @Output('mode') mode = new EventEmitter<'edit'>()
  @Output('deleteElement') elementToDelete = new EventEmitter<'Delete'>()

  @Input('selected') playlist : Playlist = {
    
    id: '123',
    name: 'Playlist',
    publicPlaylistAttr: false,
    description: 'my favourite playlist'
  }
  editOrAddMode = 'Edit'  
  ngOnChanges(): void{        

    if(this.playlist?.id != 'undefined'){
      this.editOrAddMode = 'Edit'     
    }else{
      this.editOrAddMode = 'Add'           
    }
}

  ngOnInit(): void {
  }

  deleteElement(){
    this.elementToDelete.emit('Delete')
  }

  switchToEditMode() {
    this.mode.emit('edit')
  }

}
